[@ceajs/sign-plugin](README.md) / Exports

# @ceajs/sign-plugin

## Table of contents

### Functions

- [checkIn](modules.md#checkin)

## Functions

### checkIn

▸ **checkIn**(): `Promise`<`void`\>

#### Returns

`Promise`<`void`\>

#### Defined in

[index.ts:3](https://github.com/ceajs/cea/blob/3498600/src/plugins/sign/src/index.ts#L3)
